import React, { Component } from "react"
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom"
import classNames from "classnames"
import CssBaseline from "@material-ui/core/CssBaseline"
import { withStyles } from "@material-ui/core/styles"
import Navigation from "./components/Navigation"
import SkillReport from "./pages/SkillReport"
import api from "./utils/api"
import Opportunities from "./pages/Opportunities"
import Loader from "./components/Loader"
import SignIn from "./pages/SignIn"

const styles = theme => ({
  root: {
    display: "flex"
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  loading: {
    justifyContent: "space-around"
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3
  }
})

class App extends Component {
  state = {
    auth: false,
    skills: undefined,
    users: undefined,
    opportunities: [],
    isLoading: true
  }

  componentDidMount() {
    const auth = localStorage.getItem("auth")
    if (auth) {
      this.setState({
        auth
      })
    }
    Promise.all([
      api.fetchSkills(),
      api.fetchUsers(),
      api.fetchOpportunities()
    ]).then(values => {
      const [skills, users, opportunities] = values
      this.setState({ skills })
      this.setState({ users })
      this.setState({ opportunities })
      this.setState({ isLoading: false })
    })
  }

  postOpportunities(body) {
    return api.postOpportunity(body)
  }

  addOpportunity = opp => {
    this.setState({
      opportunities: [opp, ...this.state.opportunities]
    })
  }

  renderSkillReport = () =>
    this.state.auth ? (
      <SkillReport skills={this.state.skills} users={this.state.users} />
    ) : (
      <Redirect to="/auth" />
    )

  renderOpportunities = () =>
    this.state.auth ? (
      <Opportunities
        skills={this.state.skills}
        opportunities={this.state.opportunities}
        onSubmit={this.postOpportunities}
        addOpportunity={this.addOpportunity}
        users={this.state.users}
      />
    ) : (
      <Redirect to="/auth" />
    )

  renderSignIn = () => <SignIn onLogin={this.login} auth={this.state.auth} />

  login = event => {
    event.preventDefault()

    this.setState({
      auth: true
    })

    localStorage.setItem("auth", true)
  }

  logout = event => {
    event.preventDefault()
    localStorage.removeItem("auth")
    this.setState({
      auth: false
    })
  }

  render() {
    const { classes } = this.props
    const { isLoading, auth } = this.state

    return (
      <BrowserRouter>
        <div className={classes.root}>
          <CssBaseline />
          {auth && <Navigation onLogout={this.logout} />}
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <div
              className={classNames(classes.toolbar, {
                [classes.loading]: isLoading
              })}
            >
              {isLoading ? (
                <Loader />
              ) : (
                <Switch>
                  <Route path="/auth" render={this.renderSignIn} />
                  <Route
                    path="/opportunities"
                    render={this.renderOpportunities}
                  />
                  <Route path="/" render={this.renderSkillReport} />
                </Switch>
              )}
            </div>
          </main>
        </div>
      </BrowserRouter>
    )
  }
}

export default withStyles(styles, { withTheme: true })(App)

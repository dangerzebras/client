import { clone } from "ramda"
import API_ENDPOINTS from "../constants/api.constants"

const fetchSkills = async () => {
  return fetch(API_ENDPOINTS.SKILLS)
    .then(response => {
      return response.json()
    })
    .catch(error => {
      throw error
    })
}

const fetchUsers = async () => {
  return fetch(API_ENDPOINTS.USER)
    .then(response => {
      return response.json()
    })
    .then(users => {
      return users.map(u => {
        const user = clone(u)
        user.skills = u.skills.reduce((accum, curr) => {
          accum[curr.skillId] = curr.endorsements.length
          return accum
        }, {})
        return user
      })
    })
    .catch(error => {
      throw error
    })
}

const postOpportunity = async data => {
  return fetch(API_ENDPOINTS.OPPORTUNITY, {
    method: "POST",
    body: JSON.stringify(data), // data can be `string` or {object}!
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(response => {
      return response.json()
    })
    .catch(error => {
      throw error
    })
}

const fetchOpportunities = async () => {
  return fetch(API_ENDPOINTS.OPPORTUNITY)
    .then(response => response.json())
    .catch(error => {
      throw error
    })
}

export default {
  fetchSkills,
  fetchUsers,
  fetchOpportunities,
  postOpportunity
}

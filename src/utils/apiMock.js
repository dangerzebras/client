import API_ENDPOINTS from "../constants/api.constants"

const fetchSkills = async () => {
  return new Promise(resolve =>
    setTimeout(() => {
      resolve([
        {
          id: "5be6616bdc1be3534c4c6b72",
          categoryName: "string",
          skills: [
            {
              id: "4f3556d9-76f5-4c5b-afba-75bb92b02b26",
              skillName: "string"
            }
          ]
        },
        {
          id: "b1e1e580-1daf-4ab0-8aa7-e71fa022e37a",
          categoryName: "Languages",
          skills: [
            {
              id: "74cf6cae-7d0c-4268-9673-7f4bc0584972",
              skillName: "Java"
            },
            {
              id: "37495294-81d9-4bde-83ef-1c1811940ee0",
              skillName: "JavaScript"
            },
            {
              id: "aa5d2303-b2e6-4896-ba54-ea19b3994d45",
              skillName: "CSharp"
            }
          ]
        },
        {
          id: "953b4280-d9b4-4182-be94-09b2135e6b80",
          categoryName: "Industry Experience",
          skills: [
            {
              id: "d82be1b7-86f0-4ad6-936b-261743573bef",
              skillName: "Insurance"
            },
            {
              id: "ec75ca1b-1a84-40e6-a6fa-9c5a5251ea05",
              skillName: "Banking"
            },
            {
              id: "c5b4c9b7-cd47-4bea-a443-528ebefbd682",
              skillName: "Pharmaceutical"
            },
            {
              id: "7f1e8127-4039-4012-9ba7-448a0c486459",
              skillName: "Healthcare"
            },
            {
              id: "794cc790-d647-4018-ab6e-4b4346e939b1",
              skillName: "Hi-Tech"
            },
            {
              id: "b4c048b6-58ef-4c15-8a09-4e0124ae3120",
              skillName: "Retail"
            },
            {
              id: "58e203d4-5c8b-4015-a483-c619ae058e04",
              skillName: "Public Sector"
            }
          ]
        }
      ])
    }, 200)
  )
}

const fetchUsers = async () =>
  new Promise(resolve =>
    resolve([
      {
        id: "1f70294a-6ecd-4156-9506-3a4c5132b5f8",
        userName: "lauren.hilton",
        userId: "UE08SUQ04",
        skills: [
          {
            skillId: "aa5d2303-b2e6-4896-ba54-ea19b3994d45",
            endorsements: [
              {
                userId: "UE08SUQ04",
                userName: "lauren.hilton",
                endorseDate: "2018-11-10T19:51:35.085Z"
              }
            ]
          },
          {
            skillId: "74cf6cae-7d0c-4268-9673-7f4bc0584972",
            endorsements: [
              {
                userId: "UE08SUQ04",
                userName: "lauren.hilton",
                endorseDate: "2018-11-10T19:52:18.509Z"
              }
            ]
          },
          {
            skillId: "ec75ca1b-1a84-40e6-a6fa-9c5a5251ea05",
            endorsements: [
              {
                userId: "UE08SUQ04",
                userName: "lauren.hilton",
                endorseDate: "2018-11-11T01:45:52.578Z"
              }
            ]
          },
          {
            skillId: "7f1e8127-4039-4012-9ba7-448a0c486459",
            endorsements: [
              {
                userId: "UE08SUQ04",
                userName: "lauren.hilton",
                endorseDate: "2018-11-11T01:46:08.3Z"
              }
            ]
          }
        ],
        name: null
      },
      {
        id: "fd9184ca-4903-4a27-8a20-64bf7312f1c4",
        userName: "keri.ondrus",
        userId: "UE0DUQT2N",
        skills: [
          {
            skillId: "74cf6cae-7d0c-4268-9673-7f4bc0584972",
            endorsements: [
              {
                userId: "UE08SUQ04",
                userName: "lauren.hilton",
                endorseDate: "2018-11-10T19:54:06.555Z"
              },
              {
                userId: "UE0DU3LQJ",
                userName: "despina.hartley",
                endorseDate: "2018-11-10T20:30:40.254Z"
              }
            ]
          },
          {
            skillId: "37495294-81d9-4bde-83ef-1c1811940ee0",
            endorsements: [
              {
                userId: "UE08SUQ04",
                userName: "lauren.hilton",
                endorseDate: "2018-11-10T19:55:44.159Z"
              }
            ]
          },
          {
            skillId: "794cc790-d647-4018-ab6e-4b4346e939b1",
            endorsements: [
              {
                userId: "UE0KUUB61",
                userName: "john.pipkin",
                endorseDate: "2018-11-11T01:50:21.127Z"
              }
            ]
          },
          {
            skillId: "c5b4c9b7-cd47-4bea-a443-528ebefbd682",
            endorsements: [
              {
                userId: "UE0DU3LQJ",
                userName: "despina.hartley",
                endorseDate: "2018-11-11T01:52:04.816Z"
              }
            ]
          }
        ],
        name: null
      },
      {
        id: "9b8a5322-ba89-4408-9555-909d9981a724",
        userName: "john.pipkin",
        userId: "UE0KUUB61",
        skills: [
          {
            skillId: "aa5d2303-b2e6-4896-ba54-ea19b3994d45",
            endorsements: [
              {
                userId: "UE08SUQ04",
                userName: "lauren.hilton",
                endorseDate: "2018-11-10T20:21:51.061Z"
              },
              {
                userId: "UE0DU3LQJ",
                userName: "despina.hartley",
                endorseDate: "2018-11-10T21:09:30.365Z"
              }
            ]
          },
          {
            skillId: "4f3556d9-76f5-4c5b-afba-75bb92b02b26",
            endorsements: [
              {
                userId: "UE0KUUB61",
                userName: "john.pipkin",
                endorseDate: "2018-11-11T01:40:07.192Z"
              },
              {
                userId: "UE08SUQ04",
                userName: "lauren.hilton",
                endorseDate: "2018-11-11T02:45:42.264Z"
              }
            ]
          },
          {
            skillId: "37495294-81d9-4bde-83ef-1c1811940ee0",
            endorsements: [
              {
                userId: "UE0KUUB61",
                userName: "john.pipkin",
                endorseDate: "2018-11-11T01:40:49.635Z"
              }
            ]
          },
          {
            skillId: "58e203d4-5c8b-4015-a483-c619ae058e04",
            endorsements: [
              {
                userId: "UE08SUQ04",
                userName: "lauren.hilton",
                endorseDate: "2018-11-11T02:09:37.464Z"
              }
            ]
          },
          {
            skillId: "b4c048b6-58ef-4c15-8a09-4e0124ae3120",
            endorsements: [
              {
                userId: "UE08SUQ04",
                userName: "lauren.hilton",
                endorseDate: "2018-11-11T02:13:31.948Z"
              }
            ]
          },
          {
            skillId: "794cc790-d647-4018-ab6e-4b4346e939b1",
            endorsements: [
              {
                userId: "UE08SUQ04",
                userName: "lauren.hilton",
                endorseDate: "2018-11-11T02:43:16.478Z"
              }
            ]
          }
        ],
        name: null
      },
      {
        id: "b7df6c0f-a6b0-4b0f-8588-0768b53a8263",
        userName: "alexwe",
        userId: "UE08RTV9S",
        skills: [
          {
            skillId: "4f3556d9-76f5-4c5b-afba-75bb92b02b26",
            endorsements: [
              {
                userId: "UE0DU3LQJ",
                userName: "despina.hartley",
                endorseDate: "2018-11-11T00:45:06.231Z"
              },
              {
                userId: "UE08RTV9S",
                userName: "alexwe",
                endorseDate: "2018-11-11T01:41:02.169Z"
              }
            ]
          },
          {
            skillId: "aa5d2303-b2e6-4896-ba54-ea19b3994d45",
            endorsements: [
              {
                userId: "UE0KUUB61",
                userName: "john.pipkin",
                endorseDate: "2018-11-11T01:47:22.84Z"
              }
            ]
          },
          {
            skillId: "ec75ca1b-1a84-40e6-a6fa-9c5a5251ea05",
            endorsements: [
              {
                userId: "UE0DU3LQJ",
                userName: "despina.hartley",
                endorseDate: "2018-11-11T01:51:16.443Z"
              }
            ]
          }
        ],
        name: null
      },
      {
        id: "0e678d2d-e6c0-45e3-a67a-90a35d5edbf3",
        userName: "despina.hartley",
        userId: "UE0DU3LQJ",
        skills: [
          {
            skillId: "aa5d2303-b2e6-4896-ba54-ea19b3994d45",
            endorsements: [
              {
                userId: "UE08SUQ04",
                userName: "lauren.hilton",
                endorseDate: "2018-11-11T01:51:13.355Z"
              }
            ]
          },
          {
            skillId: "74cf6cae-7d0c-4268-9673-7f4bc0584972",
            endorsements: [
              {
                userId: "UE0DU3LQJ",
                userName: "despina.hartley",
                endorseDate: "2018-11-11T02:53:30.09Z"
              }
            ]
          }
        ],
        name: null
      }
    ])
  )

const postOpportunity = async data => {
  return fetch(API_ENDPOINTS.OPPORTUNITY, {
    method: "POST",
    body: JSON.stringify(data), // data can be `string` or {object}!
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(response => {
      return response.json()
    })
    .catch(error => {
      throw error
    })
}

const fetchOpportunities = async () => {
  return new Promise(resolve =>
    resolve([
      {
        id: "0d20948e-d893-45e0-b713-74f6ede3cf5f",
        title: "Demo Test",
        description: '"Yes"',
        addedDate: "2018-11-11T05:55:16.516Z",
        skillIds: ["ec75ca1b-1a84-40e6-a6fa-9c5a5251ea05"],
        userIds: ["UE0DU3LQJ", "UE08SUQ04"]
      },
      {
        id: "b9e1aa4e-0430-4b61-be6d-f89431063169",
        title: "New Opportunity",
        description: "Looking for a winning hackathon team",
        addedDate: "2018-11-11T05:55:00.566Z",
        skillIds: [
          "37495294-81d9-4bde-83ef-1c1811940ee0",
          "aa5d2303-b2e6-4896-ba54-ea19b3994d45"
        ],
        userIds: ["UE0KUUB61"]
      },
      {
        id: "f8d64836-ce1d-4266-b109-89a9f60d6ad4",
        title: "BA for BAness",
        description: "BA for ragtag hackathon team",
        addedDate: "2018-11-11T05:52:47.139Z",
        skillIds: [
          "aa5d2303-b2e6-4896-ba54-ea19b3994d45",
          "ec75ca1b-1a84-40e6-a6fa-9c5a5251ea05",
          "794cc790-d647-4018-ab6e-4b4346e939b1"
        ],
        userIds: null
      },
      {
        id: "c065bc23-2f0b-49b9-96ce-39086e9be903",
        title: "New Demo Opportunity",
        description: "Lorem ipsum dolor sit amet.",
        addedDate: "2018-11-11T05:52:00.798Z",
        skillIds: [
          "ec75ca1b-1a84-40e6-a6fa-9c5a5251ea05",
          "aa5d2303-b2e6-4896-ba54-ea19b3994d45"
        ],
        userIds: null
      },
      {
        id: "143a1cf3-b1bb-4882-a892-b4ca7d25f5a6",
        title: "Señor Consultant",
        description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        addedDate: "2018-11-11T05:50:38.795Z",
        skillIds: [
          "7f1e8127-4039-4012-9ba7-448a0c486459",
          "794cc790-d647-4018-ab6e-4b4346e939b1",
          "c5b4c9b7-cd47-4bea-a443-528ebefbd682"
        ],
        userIds: null
      }
    ])
  )
}

export default {
  fetchSkills,
  fetchUsers,
  postOpportunity,
  fetchOpportunities
}

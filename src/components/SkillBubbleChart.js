import React from 'react';
import { ResponsiveBubble } from '@nivo/circle-packing'

const SkillBubbleChart = ({ data }) => {
  return(
    <React.Fragment>
      <ResponsiveBubble
        root={data}
        margin={{
          "top": 20,
          "right": 20,
          "bottom": 20,
          "left": 20
        }}
        identity="name"
        value="loc"
        colors="nivo"
        colorBy="depth"
        padding={6}
        labelTextColor="inherit:darker(4)"
        borderWidth={2}
        defs={[
          {
            "id": "lines",
            "type": "patternLines",
            "background": "none",
            "color": "inherit",
            "rotation": -45,
            "lineWidth": 5,
            "spacing": 8
          }
        ]}
        fill={[
          {
            "match": {
              "depth": 1
            },
            "id": "lines"
          }
        ]}
        animate={true}
        motionStiffness={90}
        motionDamping={12}
      />
    </React.Fragment>
  )
};

export default SkillBubbleChart


import React from "react"
import { ResponsiveRadar } from "@nivo/radar"
import { without, keys } from "ramda"
const RadarChart = ({ data }) => {
  const chartKeys = without(["skill"], keys(data[0]))
  return (
    <React.Fragment>
      <ResponsiveRadar
        data={data}
        keys={chartKeys}
        indexBy="skill"
        maxValue="auto"
        margin={{
          top: 80,
          right: 80,
          bottom: 40,
          left: 80
        }}
        curve="catmullRomClosed"
        borderWidth={2}
        borderColor="inherit"
        gridLevels={6}
        gridShape="circular"
        gridLabelOffset={40}
        enableDots={true}
        dotSize={10}
        dotColor="inherit"
        dotBorderWidth={0}
        dotBorderColor="#ffffff"
        enableDotLabel={true}
        dotLabel="value"
        dotLabelYOffset={-12}
        colors="category10"
        colorBy="key"
        fillOpacity={0.1}
        animate={true}
        motionStiffness={90}
        motionDamping={15}
        isInteractive={true}
        legends={[
          {
            anchor: "top-left",
            direction: "column",
            translateX: -50,
            translateY: -40,
            itemWidth: 80,
            itemHeight: 20,
            itemTextColor: "#999",
            symbolSize: 12,
            symbolShape: "circle",
            effects: [
              {
                on: "hover",
                style: {
                  itemTextColor: "#000"
                }
              }
            ]
          }
        ]}
      />
    </React.Fragment>
  )
}

export default RadarChart

import React from "react"
import { NavLink } from "react-router-dom"
import ListItem from "@material-ui/core/ListItem"
import ListItemIcon from "@material-ui/core/ListItemIcon"
import ListItemText from "@material-ui/core/ListItemText"

const NavItem = ({ url, label, children, onClick }) =>
  onClick ? (
    <ListItem button onClick={onClick} key={label}>
      <ListItemIcon>{children}</ListItemIcon>
      <ListItemText primary={label} />
    </ListItem>
  ) : (
    <NavLink to={url}>
      <ListItem button key={label}>
        <ListItemIcon>{children}</ListItemIcon>
        <ListItemText primary={label} />
      </ListItem>
    </NavLink>
  )

export default NavItem

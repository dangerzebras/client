import React from "react"
import Paper from "@material-ui/core/Paper"
import Typography from "@material-ui/core/Typography"
import Grid from "@material-ui/core/Grid"
import Chip from "@material-ui/core/Chip"
import Divider from "@material-ui/core/Divider"
import withStyles from "@material-ui/core/es/styles/withStyles"
import UserListDialog from "./opportunityCard/UserListDialog"

const styles = theme => ({
  root: {
    height: "100%",
    padding: theme.spacing.unit * 1.5
  },
  chipSpace: {
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit
  },
  gridHeight: {
    height: "100%"
  },
  spacer: {
    marginTop: `${theme.spacing.unit}px !important`,
    width: "100%"
  }
})

const OpportunityCard = ({
  opportunity,
  allSkills,
  classes,
  users,
  userIds
}) => {
  const getChip = skillId => {
    const skill = allSkills.find(s => s.id === skillId)

    return skill ? (
      <Chip
        key={skill.id}
        label={skill.skillName}
        className={classes.chipSpace}
      />
    ) : null
  }

  return (
    <Paper className={classes.root}>
      <Grid
        container
        direction="column"
        justify="space-between"
        alignItems="stretch"
        className={classes.gridHeight}
      >
        <Grid item>
          <Typography variant="h6" gutterBottom>
            {opportunity.title}
          </Typography>
          <Typography>{opportunity.description}</Typography>
        </Grid>
        <Grid item>
          {opportunity.skillIds && opportunity.skillIds.map(getChip)}

          {opportunity.userIds && (
            <div className={classes.spacer}>
              <Divider />
              <UserListDialog
                title={opportunity.title}
                users={users}
                userIds={opportunity.userIds}
              />
            </div>
          )}
        </Grid>
      </Grid>
    </Paper>
  )
}

export default withStyles(styles, { withTheme: true })(OpportunityCard)

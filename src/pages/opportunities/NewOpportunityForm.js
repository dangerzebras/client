import React from "react"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import TextField from "@material-ui/core/TextField"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import FormControl from "@material-ui/core/FormControl"
import Input from "@material-ui/core/Input"
import Checkbox from "@material-ui/core/Checkbox"
import Select from "@material-ui/core/Select"
import InputLabel from "@material-ui/core/InputLabel"
import MenuItem from "@material-ui/core/MenuItem"
import { withStyles } from "@material-ui/core/styles"
import classNames from "classnames"
import Chip from "@material-ui/core/Chip"
import Button from "@material-ui/core/Button"
import SaveIcon from "@material-ui/icons/Save"

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  },
  iconSmall: {
    fontSize: 20
  },
  chips: {
    display: "flex",
    flexWrap: "wrap"
  },
  chip: {
    margin: theme.spacing.unit / 4
  }
})

const ITEM_HEIGHT = 48
const ITEM_PADDING_TOP = 8
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
}

const NewOpportunityForm = ({
  newOpportunity,
  onSubmit,
  onChange,
  classes,
  skills
}) => {
  const isEnabled =
    newOpportunity.title.length > 0 &&
    newOpportunity.description.length > 0 &&
    newOpportunity.skillIds.length > 0

  return (
    <form onSubmit={onSubmit}>
      <Typography variant="h6" gutterBottom>
        New Opportunity
      </Typography>
      <Grid container spacing={24}>
        <Grid item xs={12}>
          <TextField
            required
            id="title"
            name="title"
            onChange={onChange}
            value={newOpportunity.title}
            fullWidth={true}
            label="Title"
            placeholder="Title"
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="description"
            name="description"
            onChange={onChange}
            value={newOpportunity.description}
            fullWidth={true}
            label="Description"
            placeholder="Description"
            multiline={true}
          />
        </Grid>
        <Grid item xs={12}>
          <FormControl className={classes.formControl} fullWidth={true}>
            <InputLabel htmlFor="skillIds" required>
              Skills
            </InputLabel>
            <Select
              multiple
              value={newOpportunity.skillIds}
              onChange={onChange}
              input={<Input id="skillIds" name="skillIds" required />}
              MenuProps={MenuProps}
              renderValue={selected => (
                <div className={classes.chips}>
                  {selected.map(skillId => {
                    const skill = skills.find(s => s.id === skillId)
                    return (
                      <Chip
                        key={skill.id}
                        label={skill.skillName}
                        className={classes.chip}
                      />
                    )
                  })}
                </div>
              )}
            >
              {skills.map(skill => (
                <MenuItem key={skill.id} value={skill.id}>
                  {skill.skillName}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControlLabel
            control={
              <Checkbox
                color="secondary"
                name="autoPost"
                checked={newOpportunity.autoPost}
                onChange={onChange}
              />
            }
            label="Auto post new opportunity to Slack"
          />
        </Grid>
      </Grid>
      <Button
        variant="contained"
        size="small"
        className={classes.button}
        type="submit"
        disabled={!isEnabled}
      >
        <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
        Save
      </Button>
    </form>
  )
}

export default withStyles(styles)(NewOpportunityForm)

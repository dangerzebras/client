import React from "react"
import Button from "@material-ui/core/Button"
import Dialog from "@material-ui/core/Dialog"
import DialogActions from "@material-ui/core/DialogActions"
import DialogContent from "@material-ui/core/DialogContent"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import DialogTitle from "@material-ui/core/DialogTitle"
import Slide from "@material-ui/core/Slide"

function Transition(props) {
  return <Slide direction="up" {...props} />
}

class UserListDialog extends React.Component {
  state = {
    open: false
  }

  handleClickOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  render() {
    const { title, users, userIds } = this.props
    return (
      <div>
        <Button onClick={this.handleClickOpen} fullWidth={true}>
          View Interested Consultants
        </Button>
        <Dialog
          open={this.state.open}
          TransitionComponent={Transition}
          keepMounted
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">
            {`Consultants interested in ${title}`}
          </DialogTitle>
          <DialogContent>
            <List>
              {userIds &&
                userIds.map(userId => {
                  const user = users.find(u => u.userId === userId)
                  return (
                    <ListItem>
                      <ListItemText primary={user.userName} />
                    </ListItem>
                  )
                })}
            </List>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default UserListDialog

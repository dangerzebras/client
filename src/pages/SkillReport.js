import React, { Component } from "react"
import Grid from "@material-ui/core/Grid"
import Card from "@material-ui/core/Card"
import CardContent from "@material-ui/core/CardContent"
import withStyles from "@material-ui/core/es/styles/withStyles"
import { without } from "ramda"
import RadarChart from "../components/RadarChart"
import UserSelect from "./skillReport/UserSelect"
import UserChip from "./skillReport/UserChip"
import SkillBubbleChart from "../components/SkillBubbleChart"
import Typography from "@material-ui/core/Typography/Typography"

// import classes from "*.module.css"

const styles = theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  chartContainer: {
    height: 500
  },
  bubbleChartContainer: {
    height: 400
  },
  infoCards: {
    height: 245
  },
  selectUserCard: {
    height: 240
  },
  spacer: {
    marginTop: theme.spacing.unit * 2
  }
})

class SkillReport extends Component {
  state = {
    selectedUsers: []
  }

  getChartData = () => {
    return this.props.skills.map(sc => {
      const chartData = {
        skill: sc.categoryName
      }
      const skillIds = sc.skills.map(s => s.id)

      for (const user of this.state.selectedUsers) {
        chartData[user.userName] = Object.keys(user.skills).reduce(
          (accum, curr) => {
            if (skillIds.includes(curr)) {
              accum += user.skills[curr]
            }
            return accum
          },
          0
        )
      }
      return chartData
    })
  }

  getBubbleChartData = user => {
    const userSkills = Object.keys(user.skills)
    let data = {
      name: user.userName,
      color: "hsl(348, 70%, 50%)",
      children: []
    }

    for (const skillCategory of this.props.skills) {
      let sc = {}
      let childSkills = []
      for (const skill of skillCategory.skills) {
        if (userSkills.includes(skill.id)) {
          childSkills.push({
            name: skill.skillName,
            color: "hsl(348, 70%, 50%)",
            loc: user.skills[skill.id]
          })
        }
      }
      if (childSkills.length > 0) {
        sc.name = skillCategory.categoryName
        sc.color = "hsl(348, 70%, 50%)"
        sc.children = childSkills

        data.children.push(sc)
      }
    }
    return data
  }

  selectUser = event => {
    this.setState({
      selectedUsers: [...this.state.selectedUsers, event.target.value]
    })
  }

  removeSelectedUser = user => event => {
    this.setState({
      selectedUsers: without([user])(this.state.selectedUsers)
    })
  }

  render() {
    const { classes, users } = this.props
    const { selectedUsers } = this.state
    return (
      <Grid container spacing={24}>
        <Grid item xs={12} sm={4}>
          <Card>
            <CardContent className={classes.infoCards}>
              <Typography variant="display1" align="center" gutterBottom={true}>
                Consultant Skill Analysis
              </Typography>
              <Typography variant="body2" gutterBottom align="justify">
                The analysis tools on this page allow Daugherty leaders to
                examine consultant skill sets and experience. The radar chart
                can be used to overlay the skill sets of up to five consultants
                to assist in determining a well-rounded team or it can be used
                to compare the skill sets of multiple consultants to find the
                best fit for an opportunity. The bubble charts display more
                details on each selected consultant's skills.
              </Typography>
            </CardContent>
          </Card>
          <p />
          <Card>
            <CardContent className={classes.selectUserCard}>
              <Typography variant="display1" align="center">
                Consultant Selection
              </Typography>
              {selectedUsers.length < 5 && (
                <UserSelect
                  key="default-user-select"
                  users={users}
                  onChange={this.selectUser}
                  selectedUsers={selectedUsers}
                />
              )}
              {selectedUsers.map(user => (
                <UserChip user={user} onDelete={this.removeSelectedUser} />
              ))}
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} sm={8}>
          <Card>
            <CardContent className={classes.chartContainer}>
              <RadarChart data={this.getChartData()} />
            </CardContent>
          </Card>
        </Grid>
        {selectedUsers.map(user => (
          <Grid item xs={12} sm={4}>
            <Card>
              <CardContent className={classes.bubbleChartContainer}>
                <Typography variant="display1" align="center">
                  {user.userName}
                </Typography>
                <SkillBubbleChart data={this.getBubbleChartData(user)} />
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    )
  }
}

export default withStyles(styles)(SkillReport)

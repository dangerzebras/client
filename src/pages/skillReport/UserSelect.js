import React from "react"
import InputLabel from "@material-ui/core/InputLabel/InputLabel"
import FormControl from "@material-ui/core/FormControl/FormControl"
import Select from "@material-ui/core/Select/Select"
import MenuItem from "@material-ui/core/MenuItem/MenuItem"
import withStyles from "@material-ui/core/es/styles/withStyles"
import { without } from "ramda"

const styles = theme => ({
  input: {
    minWidth: theme.spacing.unit * 50
  }
})

const UserSelect = ({
  users,
  onChange,
  selectedUser,
  classes,
  selectedUsers
}) => {
  const selectableUsers = without(selectedUsers, users)
  return (
    <React.Fragment>
      <FormControl className="UserSelectFormControl">
        <InputLabel htmlFor="consultant-name">Name</InputLabel>
        <Select
          className={classes.input}
          autoWidth={false}
          value={selectedUser}
          onChange={onChange}
          inputProps={{
            name: "name",
            id: "consultant-name"
          }}
        >
          {users &&
            selectableUsers.map(u => (
              <MenuItem key={u.userName} value={u}>
                {u.userName}
              </MenuItem>
            ))}
        </Select>
      </FormControl>
    </React.Fragment>
  )
}

export default withStyles(styles)(UserSelect)

import React from "react"
import Chip from "@material-ui/core/Chip"
import withStyles from "@material-ui/core/es/styles/withStyles"
const styles = () => ({
  chipStyle: {
    marginRight: "10px",
    marginTop: "10px"
  }
})
const UserChip = ({ user, onDelete, classes }) => (
  <Chip
    key={user.key}
    label={user.userName}
    onDelete={onDelete(user)}
    className={classes.chipStyle}
  />
)

export default withStyles(styles)(UserChip)

import React, { Component } from "react"
import Grid from "@material-ui/core/Grid"
import Card from "@material-ui/core/Card"
import CardContent from "@material-ui/core/CardContent"
import withStyles from "@material-ui/core/es/styles/withStyles"
import NewOpportunityForm from "./opportunities/NewOpportunityForm"
import OpportunityCard from "./opportunities/OpportunityCard"

const styles = theme => ({
  spacer: {
    marginTop: "10px !important"
  }
})

class Opportunities extends Component {
  state = {
    newOpportunity: {
      title: "",
      description: "",
      skillIds: [],
      autoPost: true
    }
  }

  onChangeHandler = event => {
    this.setState({
      newOpportunity: {
        ...this.state.newOpportunity,
        [event.target.name]:
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value
      }
    })
  }

  onSubmitHandler = event => {
    event.preventDefault()
    this.props.onSubmit(this.state.newOpportunity).then(response => {
      this.setState({
        newOpportunity: {
          title: "",
          description: "",
          skillIds: [],
          autoPost: true
        }
      })

      this.props.addOpportunity(response)
    })
  }

  render() {
    const { newOpportunity } = this.state
    const { skills, opportunities, users, classes } = this.props

    const allSkills = skills
      .reduce((accum, curr) => {
        accum = [...accum, ...curr.skills]
        return accum
      }, [])
      .sort((a, b) => {
        const aName = a.skillName.toLowerCase()
        const bName = b.skillName.toLowerCase()
        if (aName < bName) {
          return -1
        } else if (aName > bName) {
          return 1
        }
        return 0
      })

    return (
      <React.Fragment>
        <Grid container spacing={24}>
          <Grid container spacing={24}>
            <Grid item xs={12}>
              <Card>
                <CardContent>
                  <NewOpportunityForm
                    newOpportunity={newOpportunity}
                    skills={allSkills}
                    onSubmit={this.onSubmitHandler}
                    onChange={this.onChangeHandler}
                  />
                </CardContent>
              </Card>
            </Grid>
          </Grid>
          <Grid
            container
            alignItems="stretch"
            spacing={24}
            className={classes.spacer}
          >
            {opportunities &&
              opportunities.map(opp => (
                <Grid item xs={12} sm={4} key={opp.id}>
                  <OpportunityCard
                    key={opp.id}
                    opportunity={opp}
                    allSkills={allSkills}
                    users={users}
                  />
                </Grid>
              ))}
          </Grid>
        </Grid>
      </React.Fragment>
    )
  }
}

export default withStyles(styles)(Opportunities)

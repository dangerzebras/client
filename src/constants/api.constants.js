const BASE_URL = "https://spicydinosaur.azurewebsites.net/api"

const API_ENDPOINTS = {
  BASE_URL,
  SKILLS: `${BASE_URL}/skills`,
  OPPORTUNITY: `${BASE_URL}/opportunity`,
  USER: `${BASE_URL}/user`
}

export default API_ENDPOINTS
